
<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Nuevo jugador</h1>

      <form method="post" action="/jugador/store"> <!-- una vez le das a "crear" para que lo inserte después -->

        <div class="form-group">
          <label>Nombre</label>
          <input type="text" name="nombre" class="form-control">
        </div>

        <div class="form-group">
          <label>Fecha nacimiento</label>
          <input type="text" name="nacimiento" class="form-control">
        </div>

        <div class="form-group">
          <label>Puestos: </label>
          <select name="id_puesto">
            <?php foreach ($puestos as $puesto) {?>
            <option value="<?php echo $puesto->id?>">
             <?php echo $puesto->nombre ?>
           </option>
           <?php } ?>
         </select>
       </div>

       <button type="submit" class="btn btn-default">Crear</button>
     </form>
     <br>

   </div>
 </main>

 <?php require "../app/views/parts/footer.php" ?>

</body>
<?php require "../app/views/parts/scripts.php" ?>
</html>
