<!doctype html>
<html lang="es">

<head>
 <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Lista de Jugadores</h1>

      <table class="table table-striped">
        <thead>
          <tr>
            <th>Id:</th>
            <th>Nombre:</th>
            <th>Puesto:</th>
            <th>Fecha nacimiento:</th>
            <th>Acciones:</th>
        </tr>
    </thead>
    <tbody>
      <?php foreach ($jugadores as $jugador): ?>
        <tr>
         <td><?php echo $jugador->id ?></td>
         <td><?php echo $jugador->nombre ?></td>
         <td><?php echo $jugador->puesto->nombre ?></td>
         <td><?php echo $jugador->nacimiento = date('d-m-Y') ?></td>

         <!-- El listado solo es el listado. Los detalles se ven en Show. -->

         <td>
             <a class="btn btn-primary" href="/jugador/titular/<?php echo $jugador->id ?>">Titular</a>
         </td>
     </tr>
 <?php endforeach ?>
</tbody>
</table>

<?php for ($i=1; $i <= $pages; $i++) { ?>
<?php if ($i != $page): ?>
    <a href="/jugador?page=<?php echo $i ?>" class="btn">
      <?php echo $i ?>
  </a>
<?php else: ?>
    <span class="btn">
      <?php echo $i ?>
  </span>
<?php endif ?>
<?php } ?>
<br><br>

<a href="/jugador/create"> Nuevo </a>

</div>

</main><!-- /.container -->
<?php require "../app/views/parts/footer.php" ?>


</body>
<?php require "../app/views/parts/scripts.php" ?>
</html>

