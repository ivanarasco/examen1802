<!doctype html>
<html lang="es">

<head>
   <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Lista de Titulares</h1>

      <table class="table table-striped">
        <thead>
          <tr>
            <th>Id:</th>
            <th>Nombre:</th>
            <th>Puesto:</th>
            <th>Fecha nacimiento:</th>
            <th>Acciones:</th>
        </tr>
    </thead>
    <tbody>
      <?php foreach ($_SESSION['titulares'] as $titular): ?>
        <tr>
           <td><?php echo $titular->id ?></td>
           <td><?php echo $titular->nombre ?></td>
           <td><?php echo $titular->puesto->nombre ?></td>
           <td><?php echo $titular->nacimiento ?></td>
           <!-- El listado solo es el listado. Los detalles se ven en Show. -->

           <td>
               <a class="btn btn-primary" href="/jugador/remove/<?php echo $titular->id ?>">Quitar</a>
           </td>
       </tr>
   <?php endforeach ?>
</tbody>
</table>

</div>

</main><!-- /.container -->
<?php require "../app/views/parts/footer.php" ?>


</body>
<?php require "../app/views/parts/scripts.php" ?>
</html>

