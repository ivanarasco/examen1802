<?php
namespace App\Controllers;

use \App\Models\User;
/**
*
*/
class JugadorController
{

    function __construct()
    {

    }

    public function index()
    {
        if (!isset($_SESSION['titulares'])) { // si no hay titulares en sesión
                    $_SESSION['titulares'] = []; // los creamos
                }

                $pagesize = 5;
                $jugadores = User::paginate($pagesize);
                $rowCount = User::rowCount();

                $pages = ceil($rowCount / $pagesize);

                if (isset($_REQUEST['page'])) {
                    $page = (integer) $_REQUEST['page'];
                } else {
                    $page = 1;
                }

                require "../app/views/players/index.php";
            }

     public function create() // Nos llevará a la vista para crear un nuevo jugador
     {

          $puestos = \App\Models\User::puestos(); // para el select con todos los tipos.

          require "../app/views/players/create.php";
      }

      public function titular($arguments){

        $id = (int) $arguments[0];
        $jugador = User::find($id);

        if (!(isset($_SESSION['titulares'][$jugador->id]))) { // si no estaba titulado el jugador
           $_SESSION['titulares'][$jugador->id] = $jugador; // lo introducimos a la sesión titulares.
       }

       require "../app/views/titulares/index.php"; // vamos a ver todos los titulares.
   }

   public function remove($arguments) {
    $id = (int) $arguments[0];
    $jugador = User::find($id);


    if (isset($_SESSION['titulares'][$jugador->id])) { // si está ese jugador entre los titulares
        unset($_SESSION['titulares'][$jugador->id]); // lo borramos
    }
    require "../app/views/titulares/index.php";
}


    public function store() // cuando tomas los datos del jugador
    {
        $jugador = new User();


        if (!empty($_REQUEST['nombre']) && (!empty($_REQUEST['nacimiento']))){

            $jugador->nombre = $_REQUEST['nombre'];
            $jugador->nacimiento = $_REQUEST['nacimiento'];
            $jugador->id_puesto = $_REQUEST['id_puesto'];

            $jugador->insert();
            header('Location:/jugador');
        } else {
            header('Location:/jugador/create');

        }
    }


}
