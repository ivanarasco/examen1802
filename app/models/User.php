<?php
namespace App\Models;

use PDO;
use Core\Model;

class User extends Model
{

    function __construct()
    {
        //$this->nacimiento = new \DateTime($this->nacimiento);
    }

    public function __get($atributoDesconocido)
    {
        if (method_exists($this, $atributoDesconocido)) {
            $this->$atributoDesconocido = $this->$atributoDesconocido();
            return $this->$atributoDesconocido;
        } else {
            return "";
        }
    }

    public function paginate($size = 10)

    {
        if (isset($_REQUEST['page'])) {
            $page = (integer) $_REQUEST['page'];
        } else {
            $page = 1;
        }

        $offset = ($page - 1) * $size;

        $db = User::db();
        $statement = $db->prepare('SELECT * FROM jugadores LIMIT :pagesize OFFSET :offset');
        $statement->bindValue(':pagesize', $size, PDO::PARAM_INT);
        $statement->bindValue(':offset', $offset, PDO::PARAM_INT);
        $statement->execute();

        $jugadores = $statement->fetchAll(PDO::FETCH_CLASS, User::class);

        return $jugadores;
    }

    public static function rowCount()
    {
        $db = User::db();
        $statement = $db->prepare('SELECT count(id) as count FROM jugadores');
        $statement->execute();

        $rowCount = $statement->fetch(PDO::FETCH_ASSOC);
        return $rowCount['count'];
    }

    public static function puestos() // recogemos a todos los puestos para rellenar el select.
    {
        $db = User::db();
        $statement = $db->query('SELECT * FROM puestos');
        $puestos = $statement->fetchAll(PDO::FETCH_CLASS, User::class);
        // y los metemos en la variable $all, siendo un array de objetos.

        return $puestos;
    }

    public function puesto(){

       $db = User::db();
       $stmt = $db->prepare('SELECT * FROM puestos WHERE id=:id');
       $stmt->bindValue(':id', $this->id_puesto);
       $stmt->execute();
       $stmt->setFetchMode(PDO::FETCH_CLASS, User::class);
       $puesto = $stmt->fetch(PDO::FETCH_CLASS);

       return $puesto;
   }

   public static function find($id)
    // cuando necesitemos tener un jugador ya sea para borrarlo o añadirlo como titular.
   {
    $db = User::db();
    $stmt = $db->prepare('SELECT * FROM jugadores WHERE id=:id');
    $stmt->execute(array(':id' => $id));
    $stmt->setFetchMode(PDO::FETCH_CLASS, User::class);
    $jugador = $stmt->fetch(PDO::FETCH_CLASS);

    return $jugador;
}

public function insert() // para insertar un jugador
{
    $db = User::db();
    $stmt = $db->prepare('INSERT INTO jugadores (nombre, nacimiento, id_puesto) VALUES(:nombre, :nacimiento, :id_puesto)');
    $stmt->bindValue(':nombre', $this->nombre);
    $stmt->bindValue(':nacimiento', $this->nacimiento);
    $stmt->bindValue(':id_puesto', $this->id_puesto);

    return $stmt->execute();
}


}
